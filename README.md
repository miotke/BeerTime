# BeerTime
[![Review](https://img.shields.io/badge/Project-Complete-brightgreen.svg)]()
[![Review](https://img.shields.io/badge/Status-Approved-brightgreen.svg)]()<br><br><br>
<!--[![Review](https://img.shields.io/badge/Status-In Review-orange.svg)]() -->
A Amazon Echo Skill

Need help choosing a beer? Let Alexa help!<br><br>
Beer Time is an Amazon Echo skill that will help you choose at random a type of beer to drink for the night. It will also include a quick humorous addition to Alexa's choice of beer.<br><br>
More beer types will be added over time.
