'use strict';
var Alexa = require('alexa-sdk');

var APP_ID = "amzn1.ask.skill.9dbe0fe0-f6a3-4e82-806c-a217d9d7f129";

var SKILL_NAME = 'BeerTime';

/**
 * Array containing beer choices.
 */
var beerChoice = [
  "IPA, Why is IPA the coolest kid on the block? He packs variety! With over 12 new hop strains developed this year alone - it’s safe to say he’s going to continue keeping it fresh.",
  "Porter, Porter down!",
  "Stout, as in he’s a stout young man.",
  "Red, have you red about any of these beers?", //thanks Matt!
  "Lager, Lager than life",
  "Sour, I think you may actually enjoy the sour taste in your mouth", //thanks Matt!
  "Amber, Hop-along Cask-ity",
  "Ale, That’s ale well and good",
  "Saison, Tis the Saison",
  "Hefeweizen, what the hef!",
  "Cider, Little miss muffet sat on a tuffet, along came a spider and sat down becider."
];

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
        this.emit('GetFact');
    },
    'GetNewFactIntent': function () {
        this.emit('GetFact');
    },
    'GetFact': function () {
        // Get a random space fact from the beerChoice list
        var factIndex = Math.floor(Math.random() * beerChoice.length);
        var randomFact = beerChoice[factIndex];

        // Create speech output
        var speechOutput = "Here's your beer choice: " + randomFact;

        this.emit(':tellWithCard', speechOutput, SKILL_NAME, randomFact)
    },
    'AMAZON.HelpIntent': function () {
        var speechOutput = "You can say tell me a space fact, or, you can say exit... What can I help you with?";
        var reprompt = "What can I help you with?";
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', 'Goodbye!');
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', 'Goodbye!');
    }
};
